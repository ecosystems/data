__author__ = 'JoergHerbel'


import csv
import lxml.etree as et
import os
import sys

for path in [x[0] for x in os.walk(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))]:
    sys.path.append(path)

import data_to_xml as dtx


def attach_urls(lines, url_file_path):
    """
    Split list created by txt_to_list into 2 chunks according to whether a picture is available or not and attach
    available URLs

    :param lines: list of lines created by txt_to_list
    :param url_file_path: external .csv-file containing information about the the URLs to the pictures
    :return: tuple containing a list with all lists that correspond to a sample for which a picture is available and a
             list containing all lists for which no picture is available; the URLs are attached to the corresponding
             sublists as last entries
    """

    f = open(url_file_path, 'r')

    reader = csv.reader(f)

    url_dict = {}

    for row in reader:
        url_dict[row[0]] = row[1]

    index = lines[0].index('biocode.bnhm_id')

    with_url = [0]*len(url_dict)
    with_url_counter = 0
    wo_url = [0]*len(url_dict)
    wo_url_counter = 0

    for line in lines[1:]:
        if url_dict[line[index]] == 'N/A':
            wo_url[wo_url_counter] = line
            wo_url_counter += 1
        else:
            line.append(url_dict[line[index]])
            with_url[with_url_counter] = line
            with_url_counter += 1

    if 0 in with_url:
        with_url = with_url[:with_url.index(0)]

    if 0 in wo_url:
        wo_url = wo_url[:wo_url.index(0)]

    f.close()

    return with_url, wo_url


def build_dates(timestamps):
    """
    Create dates of the format '2010-12-23' if possible

    :param timestamps: 2-tuple of strings in the format '(2)3/(1)2/10....' or '\N' or ''
    :return: 2-tuple of strings in the correct format or False if no corresponding date is available
    """

    def datebuilder(timestamp):
        """
        Create date of the format '2010-12-23'

        :param timestamp: unicode string in the format '(2)3/(1)2/10....'
        :return: string in correct format
        """

        if timestamp[1] == '/':
            month = '0' + timestamp[0]
            if timestamp[3] == '/':
                day = '0' + timestamp[2]
                year = '20' + timestamp[4:6]
            else:
                day = timestamp[2:4]
                year = '20' + timestamp[5:7]

        else:
            month = timestamp[:2]
            if timestamp[4] == '/':
                day = '0' + timestamp[3]
                year = '20' + timestamp[5:7]
            else:
                day = timestamp[3:5]
                year = '20' + timestamp[6:8]

        return year + '-' + month + '-' + day

    date_booleans = (dtx.check_value(timestamps, 0), dtx.check_value(timestamps, 1))

    if date_booleans == (True, True):
        return datebuilder(timestamps[0]), datebuilder(timestamps[1])
    elif date_booleans[0]:
        return datebuilder(timestamps[0]), False
    elif date_booleans[1]:
        return False, datebuilder(timestamps[1])
    else:
        return False, False


def group_chunk(chunk_lines, organization_name_index, begin_date_index, end_date_index, indices):
    """
    Group lines in a chunk of data according to organisation names, dates and non-empty resp. available entries

    :param chunk_lines: a list of lines belonging to one chunk of data (i.e. the geographical location is the same
                        in all lines)
    :param organization_name_index: index of name of organization
    :param begin_date_index: index of start date
    :param end_date_index: index of end date
    :param indices: further indices which should be used to group lines
    :return: list of the format [[organization, [[(date-tuple), (indices evaluating to useful entries), [corresponding
             lines]], [(date-tuple), (indices evaluating to useful entries), [corresponding lines]],...], [organization,
             [[(date-tuple), (indices evaluating to useful entries), [corresponding lines]], [(date-tuple), (indices
             evaluating to useful entries), [corresponding lines]],...],...]; the organization name is always the same
             within all lines in one sublist
    """

    tuples = set()

    for line in chunk_lines:
        tuples.add((line[organization_name_index], build_dates((line[begin_date_index], line[end_date_index])),
                    tuple([index for index in indices if dtx.check_value(line, index)])))

    tuples = list(tuples)
    num_tuples = len(tuples)

    combinations = [[t[0], t[1], t[2], [0]*(len(chunk_lines)-num_tuples+1)] for t in tuples]

    counters1 = [0]*num_tuples

    for line in chunk_lines:
        i = tuples.index((line[organization_name_index], build_dates((line[begin_date_index], line[end_date_index])),
                          tuple([index for index in indices if dtx.check_value(line, index)])))
        combinations[i][3][counters1[i]] = line
        counters1[i] += 1

    org_names = set()

    for combination in combinations:
        org_names.add(combination[0])

    org_names = list(org_names)
    num_names = len(org_names)

    splitted_combinations = [[org_name, [0]*(num_tuples-num_names+1)] for org_name in org_names]
    counters2 = [0]*num_names

    for k in xrange(num_tuples):
        combinations[k][3] = combinations[k][3][:counters1[k]]

        i = org_names.index(combinations[k][0])
        splitted_combinations[i][1][counters2[i]] = combinations[k][1:]
        counters2[i] += 1

    for i in xrange(num_names):
        splitted_combinations[i][1] = splitted_combinations[i][1][:counters2[i]]

    return splitted_combinations


def create_xml_txt(input_file, file_pic_urls, attributes, output_path_xml, output_path_txt):
    """
    Create XML and txt-files from an input text file

    :param input_file: location of input file
    :param file_pic_urls: external CSV-file containing information about the the URLs to the pictures
    :param attributes: attributes of samples to be potentially included in the output; list of tuples where each tuple
                       has the format
                       (Variable name (see input file, e.g. 'biocode.bnhm_id'), attributeLabel, attributeDefintion)
                       and represents one attribute
    :param output_path_xml: output location of XML files
    :param output_path_txt: output location of txt files
    :return: None
    """

    lines = dtx.txt_to_list(input_file, 'ISO-8859-1')

    with_pic, wo_pic = attach_urls(lines, file_pic_urls)

    organization_name_index = lines[0].index('biocode.HoldingInstitution')

    longitude_index = lines[0].index('biocode_collecting_event.DecimalLongitude')
    latitude_index = lines[0].index('biocode_collecting_event.DecimalLatitude')

    begin_date_index = lines[0].index('biocode_collecting_event.DateFirstEntered')
    end_date_index = lines[0].index('biocode_collecting_event.DateLastModified')

    attributes_indices = [lines[0].index(attribute[0]) for attribute in attributes]

    with_pic_chunks = dtx.create_chunks(with_pic, [longitude_index, latitude_index])
    wo_pic_chunks = dtx.create_chunks(wo_pic, [longitude_index, latitude_index])

    eml = 'eml://ecoinformatics.org/eml-2.1.1'
    stmml = 'http://www.xml-cml.org/schema/stmml-1.1'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'
    schema_location = 'eml://ecoinformatics.org/eml-2.1.1 http://nis.lternet.edu/schemas/EML/eml-2.1.1/eml.xsd'

    def write_file(subchunk, has_pic, output_folder, output_name):
        """
        Create text file corresponding to one subchunk within the output of group_chunk (i.e. to [organization,
        [[(date-tuple), (indices evaluating to useful entries), [corresponding lines]],
        [(date-tuple), (indices evaluating to useful entries), [corresponding lines]],...]

        :param subchunk: subchunk used to create output file
        :param has_pic: True if and only if pictures are available for the samples corresponding to the lines in the
                        subchunk
        :param output_folder: location where text output files are stored
        :param output_name: filename of output file
        :return: None
        """

        def write_subchunk(output_path):
            """
            Write all lines within one subchunk to a text file

            :param output_path: location of text file to be created
            :return: None
            """

            txt_file = open(output_path, 'w')

            for t in subchunk[1][:-1]:
                for line in t[2]:
                    txt_file.write(dtx.build_line(line, t[1]).encode('UTF-8'))

            for line in subchunk[1][-1][2][:-1]:
                txt_file.write(dtx.build_line(line, subchunk[1][-1][1]).encode('UTF-8'))

            txt_file.write(dtx.build_line(subchunk[1][-1][2][-1], subchunk[1][-1][1])[:-1].encode('UTF-8'))

            txt_file.close()

            return

        if has_pic:
            write_subchunk(output_folder+'with_pic/'+output_name)
        else:
            write_subchunk(output_folder+'without_pic/'+output_name)

        return

    def process_chunks(chunks, has_pic):
        """
        Create XML and text files for a list of chunks (grouped by location)

        :param chunks: list of chunks created by function create_chunks
        :param has_pic: True if and only if pictures are available for the samples corresponding to the lines in the
                        chunks
        :return: None
        """

        for chunk in chunks:
            longitude_condition = dtx.check_value(chunk[0], 0)
            latitude_condition = dtx.check_value(chunk[0], 1)

            subchunks = group_chunk(chunk[1], organization_name_index, begin_date_index, end_date_index,
                                    attributes_indices)

            enumeration_condition = len(subchunks) > 1

            for i in xrange(len(subchunks)):
                eml_el = et.Element('{'+eml+'}eml', attrib={"{" + xsi + "}schemaLocation": schema_location},
                                     nsmap={'eml': eml, 'stmml': stmml, 'xsi': xsi}, packageId='moorea.idea.template',
                                     scope='system', system='moorea')

                name = 'biocode_collecting_event.DecimalLongitude_' + chunk[0][0].encode('UTF-8') \
                       + '_biocode_collecting_event.DecimalLatitude_' + chunk[0][1].encode('UTF-8')

                if enumeration_condition:
                    name = name + '_' + str(i)

                organization_name_condition = dtx.check_value(subchunks[i], 0)

                for t in subchunks[i][1]:
                    dataset_el = et.SubElement(eml_el, 'dataset')

                    if organization_name_condition:
                        creator_el = et.SubElement(dataset_el, 'creator')
                        organization_name_el = et.SubElement(creator_el, 'organizationName')
                        organization_name_el.text = subchunks[i][0]

                    abstract_el = et.SubElement(dataset_el, 'abstract')
                    para_el = et.SubElement(abstract_el, 'para')
                    para_el.text = 'Data collected by Moorea Biocode'

                    extent_el = et.SubElement(dataset_el, 'extent')
                    extent_el.text = str(len(t[2]))

                    if longitude_condition or latitude_condition or t[0][0] or t[0][1]:

                        coverage_el = et.SubElement(dataset_el, 'coverage')

                        if longitude_condition or latitude_condition:
                            geographic_coverage_el = et.SubElement(coverage_el, 'geographicCoverage')
                            bounding_coordinates_el = et.SubElement(geographic_coverage_el, 'boundingCoordinates')
                            if longitude_condition:
                                west_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                             'westBoundingCoordinate')
                                west_bounding_coordinate_el.text = chunk[0][0]
                                east_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                             'eastBoundingCoordinate')
                                east_bounding_coordinate_el.text = west_bounding_coordinate_el.text
                            if latitude_condition:
                                north_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                              'northBoundingCoordinate')
                                north_bounding_coordinate_el.text = chunk[0][1]
                                south_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                              'southBoundingCoordinate')
                                south_bounding_coordinate_el.text = north_bounding_coordinate_el.text

                        if t[0][0] or t[0][1]:
                            temporal_coverage_el = et.SubElement(coverage_el, 'temporalCoverage')
                            range_of_dates_el = et.SubElement(temporal_coverage_el, 'rangeOfDates')
                            begin_date_el = et.SubElement(range_of_dates_el, 'beginDate')
                            calendar_date_begin_el = et.SubElement(begin_date_el, 'calendarDate')
                            end_date_el = et.SubElement(range_of_dates_el, 'endDate')
                            calendar_date_end_el = et.SubElement(end_date_el, 'calendarDate')
                            if t[0][0] and t[0][1]:
                                calendar_date_begin_el.text = t[0][0]
                                calendar_date_end_el.text = t[0][1]
                            elif t[0][0]:
                                calendar_date_begin_el.text = t[0][0]
                                calendar_date_end_el.text = calendar_date_begin_el.text
                            else:
                                calendar_date_begin_el.text = t[0][1]
                                calendar_date_end_el.text = calendar_date_begin_el.text

                    datatable_el = et.SubElement(dataset_el, 'dataTable')

                    physical_el = et.SubElement(datatable_el, 'physical')
                    object_name_el = et.SubElement(physical_el, 'objectName')
                    object_name_el.text = name + '.txt'

                    attributelist_el = et.SubElement(datatable_el, 'attributeList')

                    for k in t[1]:
                        j = attributes_indices.index(k)
                        attribute_el = et.SubElement(attributelist_el, 'attribute')
                        attribute_name_el = et.SubElement(attribute_el, 'attributeName')
                        attribute_name_el.text = attributes[j][0]
                        attribute_label_el = et.SubElement(attribute_el, 'attributeLabel')
                        attribute_label_el.text = attributes[j][1]
                        attribute_definition_el = et.SubElement(attribute_el, 'attributeDefinition')
                        attribute_definition_el.text = attributes[j][2]

                    if has_pic:
                        attribute_link_description_el = et.SubElement(attributelist_el, 'attribute')
                        attribute_name_link_description_el = et.SubElement(attribute_link_description_el,
                                                                            'attributeName')
                        attribute_name_link_description_el.text = 'biocode.linkPicture'
                        attribute_label_link_description_el = et.SubElement(attribute_link_description_el,
                                                                             'attributeLabel')
                        attribute_label_link_description_el.text = 'Link'
                        attribute_definition_link_description_el = et.SubElement(attribute_link_description_el,
                                                                                  'attributeDefinition')
                        attribute_definition_link_description_el.text = 'Link to picture of specimen'

                # Build tree
                if has_pic:
                    et.ElementTree(eml_el).write(output_path_xml+'with_pic/'+name+'.xml', encoding='UTF-8',
                                                  pretty_print=True, xml_declaration=True)
                else:
                    et.ElementTree(eml_el).write(output_path_xml+'without_pic/'+name+'.xml', encoding='UTF-8',
                                                  pretty_print=True, xml_declaration=True)

                # Write file
                write_file(subchunks[i], has_pic, output_path_txt, object_name_el.text)

        return

    process_chunks(with_pic_chunks, True)
    process_chunks(wo_pic_chunks, False)

    return
