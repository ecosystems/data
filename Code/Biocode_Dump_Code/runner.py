__author__ = 'JoergHerbel'

"""
This script creates XML- and txt-files grouped by location from one single txt-file containing Biocode data. Before this
script can be run, one first has to run 'get_pic_urls.py' once in order to get the URLs to the pictures of the samples
(if available).
"""


import os
import process_txt_file

# Path of input file containing Biocode data
input_file = '../../Input/Biocode_Dump_Input/biocode.dump.dos.txt'

# Path of file containing the picture URLs
file_pic_urls = '../../Output/Biocode_Dump_Output/pictureURLs.csv'

# Directory where XML files will be stored
output_path_xml = '../../Output/Biocode_Dump_Output/XML_output/'

# Directory where txt files will be stored
output_path_txt = '../../Output/Biocode_Dump_Output/TXT_output/'

# Attributes to be included in the XML files
attributes = [('biocode.bnhm_id', 'Biocode No.', 'No. assigned to specimen by Biocode'),
              ('biocode.ScientificName', 'Scientific Name', 'Scientific Name of species'),
              ('biocode.Weight', 'Weight', 'Weight of specimen'),
              ('biocode.WeightUnits', 'Weight unit', 'Unit of weight of specimen'),
              ('biocode.Length', 'Length', 'Length of specimen'),
              ('biocode.LengthUnits', 'Length unit', 'Unit of length of specimen'),
              ('biocode.SexCaste', 'Sex', 'Sex of specimen'),
              ('biocode.color', 'Color', 'Color'),
              ('biocode.LifeStage', 'Life stage', 'Life stage of specimen'),
              ('biocode.specimen_Habitat', 'Habitat', 'Habitat of specimen'),
              ('biocode.specimen_MicroHabitat', 'Microhabitat', 'Microhabitat of specimen'),
              ('biocode_collecting_event.MinElevationMeters', 'Minimum elevation', 'Minimum elevation in meters'),
              ('biocode_collecting_event.MaxElevationMeters', 'Maximum elevation', 'Maximum elevation in meters'),
              ('biocode_collecting_event.MinDepthMeters', 'Minimum depth', 'Minimum depth in meters'),
              ('biocode_collecting_event.MaxDepthMeters', 'Maximum depth', 'Maximum depth in meters'),
              ('biocode_collecting_event.DepthOfBottomMeters', 'Depth of bottom', 'Depth of bottom in meters'),
              ('biocode_collecting_event.DepthErrorMeters', 'Depth error', 'Depth error in meters'),
              ('biocode_collecting_event.YearCollected', 'Year collected', 'Year in which specimen was collected'),
              ('biocode_collecting_event.MonthCollected', 'Month collected', 'Month in which specimen was collected'),
              ('biocode_collecting_event.DayCollected', 'Day collected', 'Day the specimen was collected'),
              ('biocode_collecting_event.TimeofDay', 'Time of day collected', 'Time of day the specimen was collected')]


if not os.path.exists(output_path_xml+'with_pic/'):
    os.makedirs(output_path_xml+'with_pic/')

if not os.path.exists(output_path_xml+'without_pic/'):
    os.makedirs(output_path_xml+'without_pic/')

if not os.path.exists(output_path_txt+'with_pic/'):
    os.makedirs(output_path_txt+'with_pic/')

if not os.path.exists(output_path_txt+'without_pic/'):
    os.makedirs(output_path_txt+'without_pic/')

process_txt_file.create_xml_txt(input_file, file_pic_urls, attributes, output_path_xml, output_path_txt)