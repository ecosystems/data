__author__ = 'JoergHerbel'

"""
This script checks if for Biocode data given as a txt-file, pictures of the samples are available and writes the URLs of
available pictures to a CSV-file. The first column of the file contains the sample identifier (the field
'biocode.bnhm_id' in the input file), the second column contains the URLs.
"""

# Path of input file
input_file = '../../Input/Biocode_Dump_Input/biocode.dump.dos.txt'

# Path of output file
output_file = '../../Output/Biocode_Dump_Output/pictureURLs.csv'


import requests
from bs4 import BeautifulSoup
import csv
import os
import sys

for path in [x[0] for x in os.walk(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))]:
    sys.path.append(path)

import data_to_xml as dtx


lines = dtx.txt_to_list(input_file, 'ISO-8859-1')

index = lines[0].index('biocode.bnhm_id')

f = open(output_file, 'w')
w = csv.writer(f)

for l in lines[1:]:
    ident = l[index]
    r = requests.get('http://biocode.berkeley.edu/cgi/biocode_query?bnhm_id=' + ident + '&one=T')

    if r.status_code == 404:
        s = [ident, 'N/A']

    else:
        soup = BeautifulSoup(r.content, "lxml")
        image_tag = soup.find_all('img')

        if not image_tag:
            s = [ident, 'N/A']
        else:
            url = image_tag[0]['src'].encode('UTF-8').replace('128x192', '512x768')
            if url.startswith('http://biocode.berkeley.edu/imgs/512x768/4444_4444/'):
                s = [ident, url]
            else:
                s = [ident, 'N/A']

    w.writerow(s)

f.close()