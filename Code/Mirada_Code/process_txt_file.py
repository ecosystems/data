__author__ = 'JoergHerbel'


import lxml.etree as et
import os
import sys

for path in [x[0] for x in os.walk(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))]:
    sys.path.append(path)

import data_to_xml as dtx


def get_columns(lines):

    num_columns = len(lines[0])
    len_column = len(lines)

    columns = [0]*num_columns

    for i in xrange(num_columns):
        columns[i] = [0]*len_column
        c = columns[i]

        for j in xrange(len_column):
            c[j] = lines[j][i]

    return columns


def build_date(timestamp):
    """
    Create date of the format '2010-12-23' if possible

    :param timestamp: string in the format '20009-05-09....' or '' or ' '
    :return: string in the correct format or False if no corresponding date is available
    """

    if dtx.check_value([timestamp], 0):
        return timestamp[:10]
    else:
        return False


def create_xml_txt(input_file, attributes, output_path_xml, output_path_txt):
    """
    Create XML and txt-files from an input text file

    :param input_file: location of input file
    :param attributes: attributes of samples to be potentially included in the output; list of tuples where each
                       tuple has the format
                       (Variable Name (see input file), attributeLabel, attributeDefintion, boolean specifying whether
                        to include the units of the variable in the XML files)
                       and represents one attribute
    :param output_path_xml: output location of XML files
    :param output_path_txt: output location of txt files
    :return: None
    """

    lines = dtx.txt_to_list(input_file, 'UTF-8')

    columns = get_columns(lines)

    longitude_index = columns[1].index('lon')
    latitude_index = columns[1].index('lat')

    date_index = columns[1].index('collection_date')

    attributes_indices = [columns[1].index(attribute[0]) for attribute in attributes]
    num_attributes = len(attributes_indices)

    eml = 'eml://ecoinformatics.org/eml-2.1.1'
    stmml = 'http://www.xml-cml.org/schema/stmml-1.1'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'
    schema_location = 'eml://ecoinformatics.org/eml-2.1.1 http://nis.lternet.edu/schemas/EML/eml-2.1.1/eml.xsd'

    for i in range(len(columns))[4:]:
        col = columns[i]
        ind = []

        longitude_condition = dtx.check_value(col, longitude_index)
        latitude_condition = dtx.check_value(col, latitude_index)
        date = build_date(col[date_index])

        name = col[0].encode('UTF-8')
        
        eml_el = et.Element('{'+eml+'}eml', attrib={"{" + xsi + "}schemaLocation": schema_location},
                             nsmap={'eml': eml, 'stmml': stmml, 'xsi': xsi}, packageId='moorea.idea.template',
                             scope='system', system='moorea')

        dataset_el = et.SubElement(eml_el, 'dataset')

        creator_el = et.SubElement(dataset_el, 'creator')
        organization_name_el = et.SubElement(creator_el, 'organizationName')
        organization_name_el.text = 'Moorea Coral Reef LTER'

        abstract_el = et.SubElement(dataset_el, 'abstract')
        para_el = et.SubElement(abstract_el, 'para')
        para_el.text = 'Data collected for MIRADA project'

        if longitude_condition or latitude_condition or date:
            coverage_el = et.SubElement(dataset_el, 'coverage')

            if longitude_condition or latitude_condition:
                geographic_coverage_el = et.SubElement(coverage_el, 'geographicCoverage')
                bounding_coordinates_el = et.SubElement(geographic_coverage_el, 'boundingCoordinates')
                if longitude_condition:
                    west_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'westBoundingCoordinate')
                    west_bounding_coordinate_el.text = col[longitude_index]
                    east_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'eastBoundingCoordinate')
                    east_bounding_coordinate_el.text = west_bounding_coordinate_el.text
                if latitude_condition:
                    north_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                  'northBoundingCoordinate')
                    north_bounding_coordinate_el.text = col[latitude_index]
                    south_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                  'southBoundingCoordinate')
                    south_bounding_coordinate_el.text = north_bounding_coordinate_el.text

            if date:
                temporal_coverage_el = et.SubElement(coverage_el, 'temporalCoverage')
                range_of_dates_el = et.SubElement(temporal_coverage_el, 'rangeOfDates')
                begin_date_el = et.SubElement(range_of_dates_el, 'beginDate')
                calendar_date_begin_el = et.SubElement(begin_date_el, 'calendarDate')
                calendar_date_begin_el.text = date
                end_date_el = et.SubElement(range_of_dates_el, 'endDate')
                calendar_date_end_el = et.SubElement(end_date_el, 'calendarDate')
                calendar_date_end_el.text = calendar_date_begin_el.text

        datatable_el = et.SubElement(dataset_el, 'dataTable')

        physical_el = et.SubElement(datatable_el, 'physical')
        object_name_el = et.SubElement(physical_el, 'objectName')
        object_name_el.text = name + '.txt'

        attributelist_el = et.SubElement(datatable_el, 'attributeList')

        for j in xrange(num_attributes):
            if dtx.check_value(col, attributes_indices[j]):
                ind.append(attributes_indices[j])
                attribute_el = et.SubElement(attributelist_el, 'attribute')
                attribute_name_el = et.SubElement(attribute_el, 'attributeName')
                attribute_name_el.text = attributes[j][0]
                attribute_label_el = et.SubElement(attribute_el, 'attributeLabel')
                attribute_label_el.text = attributes[j][1]
                attribute_definition_el = et.SubElement(attribute_el, 'attributeDefinition')
                attribute_definition_el.text = attributes[j][2]
                if attributes[j][3]:
                    attribute_definition_el.text += '; unit: ' + columns[2][attributes_indices[j]]

        et.ElementTree(eml_el).write(output_path_xml+name+'.xml', encoding='UTF-8', pretty_print=True,
                                      xml_declaration=True)

        txt_file = open(output_path_txt+object_name_el.text, 'w')
        txt_file.write(dtx.build_line(col, ind)[:-1].encode('UTF-8'))
        txt_file.close()

    return


# Code for grouping according to location:
"""
def group_chunk(chunk_columns, date_index, indices):

    Group columns in a chunk of data according to date and non-empty resp. available entries

    :param chunk_columns: a list of columns belonging to one chunk of data (i.e. the geographical location is the same
                          in all columns)
    :param begin_date_index: index of start date
    :param end_date_index: index of end date
    :param indices: further indices which should be used to group columns
    :return: list of the format [[date/False, (indices evaluating to useful entries), [corresponding columns]],
                                 [date/False, (indices evaluating to useful entries), [corresponding columns]], ...]


    tuples = set()

    for column in chunk_columns:
        tuples.add((build_date(column[date_index]),
                    tuple([index for index in indices if gen.check_value(column, index)])))

    tuples = list(tuples)
    num_tuples = len(tuples)

    combinations = [[t[0], t[1], [0]*(len(chunk_columns)-num_tuples+1)] for t in tuples]

    counters = [0]*num_tuples

    for column in chunk_columns:
        i = tuples.index((build_date(column[date_index]),
                          tuple([index for index in indices if gen.check_value(column, index)])))
        combinations[i][2][counters[i]] = column
        counters[i] += 1

    for k in xrange(num_tuples):
        combinations[k][2] = combinations[k][2][:counters[k]]

    return combinations


def create_xml_txt(input_file, attributes, output_path_xml, output_path_txt):

    Create XML and txt-files from an input text file

    :param input_file: location of input file
    :param attributes: attributes of samples to be potentially included in the output
    :param output_path_xml: output location of XML files
    :param output_path_txt: output location of txt files
    :return: None


    lines = gen.txt_to_list(input_file, 'UTF-8')

    columns = get_columns(lines)

    longitude_index = columns[1].index('lon')
    latitude_index = columns[1].index('lat')

    date_index = columns[1].index('collection_date')

    attributes_indices = [columns[1].index(attribute[0]) for attribute in attributes]

    chunks = gen.create_chunks(columns[4:], [longitude_index, latitude_index])

    eml = 'eml://ecoinformatics.org/eml-2.1.1'
    stmml = 'http://www.xml-cml.org/schema/stmml-1.1'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'
    schema_location = 'eml://ecoinformatics.org/eml-2.1.1 http://nis.lternet.edu/schemas/EML/eml-2.1.1/eml.xsd'

    def write_file(subc, output_path):

        Create text file corresponding to one subchunk within the output of group_chunk, i.e. to [[date/False,
        (indices evaluating to useful entries), [corresponding columns]], [date/False,
        (indices evaluating to useful entries), [corresponding columns]], ...]

        :param subc: subchunk used to create output file
        :param output_path: location and name of output
        :return: None


        def build_line(col, index_list):

            Build a unicode string from a list of unicode strings

            :param col: list containing unicode srings from which string should be built
            :param index_list: indices of strings that should be used to build string
            :return: string containing indexed strings separated tabs, if available a link to a picture at the end and
                     a linebreak


            built_line = u''

            for n in index_list[:-1]:
                built_line += col[n] + u'\t'

            built_line += col[index_list[-1]] + u'\n'

            return built_line

        txt_file = open(output_path, 'w')

        for column in subc[2][:-1]:
            txt_file.write(build_line(column, subc[1]).encode('UTF-8'))

        txt_file.write(build_line(subc[2][-1], subc[1])[:-1].encode('UTF-8'))

        txt_file.close()

        return

    for chunk in chunks:
        longitude_condition = gen.check_value(chunk[0], 0)
        latitude_condition = gen.check_value(chunk[0], 1)

        subchunks = group_chunk(chunk[1], date_index, attributes_indices)

        for subchunk in subchunks:

            name = 'lon_' + chunk[0][0].encode('UTF-8') + '_lat_' + chunk[0][1].encode('UTF-8')

            eml_el = et.Element('{'+eml+'}eml', attrib={"{" + xsi + "}schemaLocation": schema_location},
                                 nsmap={'eml': eml, 'stmml': stmml, 'xsi': xsi}, packageId='moorea.idea.template',
                                 scope='system', system='moorea')

            dataset_el = et.SubElement(eml_el, 'dataset')

            creator_el = et.SubElement(dataset_el, 'creator')
            organization_name_el = et.SubElement(creator_el, 'organizationName')
            organization_name_el.text = 'Moorea Coral Reef LTER'

            abstract_el = et.SubElement(dataset_el, 'abstract')
            para_el = et.SubElement(abstract_el, 'para')
            para_el.text = 'Data collected for MIRADA project'

            extent_el = et.SubElement(dataset_el, 'extent')
            extent_el.text = str(len(subchunk[2]))

            if longitude_condition or latitude_condition or subchunk[0]:
                coverage_el = et.SubElement(dataset_el, 'coverage')

                if longitude_condition or latitude_condition:
                    geographic_coverage_el = et.SubElement(coverage_el, 'geographicCoverage')
                    bounding_coordinates_el = et.SubElement(geographic_coverage_el, 'boundingCoordinates')
                    if longitude_condition:
                        west_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'westBoundingCoordinate')
                        west_bounding_coordinate_el.text = chunk[0][0]
                        east_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'eastBoundingCoordinate')
                        east_bounding_coordinate_el.text = west_bounding_coordinate_el.text
                    if latitude_condition:
                        north_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                      'northBoundingCoordinate')
                        north_bounding_coordinate_el.text = chunk[0][1]
                        south_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                      'southBoundingCoordinate')
                        south_bounding_coordinate_el.text = north_bounding_coordinate_el.text

                if subchunk[0]:
                    temporal_coverage_el = et.SubElement(coverage_el, 'temporalCoverage')
                    range_of_dates_el = et.SubElement(temporal_coverage_el, 'rangeOfDates')
                    begin_date_el = et.SubElement(range_of_dates_el, 'beginDate')
                    calendar_date_begin_el = et.SubElement(begin_date_el, 'calendarDate')
                    calendar_date_begin_el.text = subchunk[0]
                    end_date_el = et.SubElement(range_of_dates_el, 'endDate')
                    calendar_date_end_el = et.SubElement(end_date_el, 'calendarDate')
                    calendar_date_end_el.text = calendar_date_begin_el.text

            datatable_el = et.SubElement(dataset_el, 'dataTable')

            physical_el = et.SubElement(datatable_el, 'physical')
            object_name_el = et.SubElement(physical_el, 'objectName')
            object_name_el.text = name + '.txt'

            attributelist_el = et.SubElement(datatable_el, 'attributeList')

            for k in subchunk[1]:
                j = attributes_indices.index(k)
                attribute_el = et.SubElement(attributelist_el, 'attribute')
                attribute_name_el = et.SubElement(attribute_el, 'attributeName')
                attribute_name_el.text = attributes[j][0]
                attribute_label_el = et.SubElement(attribute_el, 'attributeLabel')
                attribute_label_el.text = attributes[j][1]
                attribute_definition_el = et.SubElement(attribute_el, 'attributeDefinition')
                attribute_definition_el.text = attributes[j][2]
                if attributes[j][3]:
                    attribute_definition_el.text += '; unit: ' + columns[2][k]

            et.ElementTree(eml_el).write(output_path_xml+name+'.xml', encoding='UTF-8', pretty_print=True,
                                          xml_declaration=True)
            write_file(subchunk, output_path_txt+object_name_el.text)

    return
"""
