__author__ = 'JoergHerbel'

"""
This script creates XML- and txt-files from one single txt-file containing Mirada data
"""


import os
import process_txt_file

# Path of input file containing Mirada data
input_file = '../../Input/Mirada_Input/LTR_MCR_env_data.txt'

# Directory where XML files will be stored
output_path_xml = '../../Output/Mirada_Output/XML_output/'

# Directory where txt files will be stored
output_path_txt = '../../Output/Mirada_Output/TXT_output/'

# Attributes to be included in the XML files
attributes = [('sample_id', 'ID', 'ID of sample', False),
              ('sample_type', 'Type', 'Type of sample', False),
              ('samp_size', 'Size', 'Size of sample', True),
              ('depth', 'Depth', 'Depth', True),
              ('depth_start', 'Start depth', 'Start depth', True),
              ('depth_end', 'End depth', 'End depth', True),
              ('envo_biome', 'Environemnt', 'Environemnt of sample', False),
              ('temp', 'Temperature', 'Temperature', True),
              ('diss_oxygen', 'Oxygen content', 'Oxygen content of sample', True),
              ('fluorescence', 'Fluorescence', 'Fluorescence of sample', True),
              ('organism_count', 'Organism density', 'Organism density of sample', True),
              ('par_irradiance', 'Irradiance', 'Irradiance of sample', True),
              ('salinity', 'Salinity', 'Salinity of sample', True),
              ('url', 'URLs', 'Relevant URLs', False)]

if not os.path.exists(output_path_xml):
    os.makedirs(output_path_xml)

if not os.path.exists(output_path_txt):
    os.makedirs(output_path_txt)

process_txt_file.create_xml_txt(input_file, attributes, output_path_xml, output_path_txt)
