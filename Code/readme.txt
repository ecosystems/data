General information about the code structure:

- For each distinct data (CRIOBE, LTER, Biocode,...), there is one sub-directory

- In each sub-directory, there several Python scripts (written using Python 2.7.10); there is always a file called
  'runner.py' or several files called runner_*.py. These combine the functionalities of the other scripts to generate
  the output files (when run from the command line, for example). These files contain further comments about the usage
  of the specific code.

- Most of the functions defined and used in the scripts contain documentation

- The directory 'scripts' contains scripts from which functions are imported into the code for the different datasets

- Necessary (external) Python packages to run all scripts (all available via pip, see https://pypi.python.org/pypi/pip):
  - lxml
  - NumPy
  - Beautiful Soup 4
