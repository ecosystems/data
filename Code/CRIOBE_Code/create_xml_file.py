__author__ = 'JoergHerbel'

import lxml.etree as et


def create_xml_file(output_file_path, abstract, data, west_coord=None, east_coord=None, north_coord=None,
                    south_coord=None):
    """
    Create an XML file containing CRIOBE metadata

    :param output_file_path: path of output file
    :param abstract: abstract
    :param data: Dictionary (one dataset) or list of dictionaries (several datasets) representing metadata to be written
                 to the XML file; format of each dictionary:
                 {'organization_name': '...', 'begin_date': '...', 'end_date': '...', 'object_name': '...',
                 'attributes_list': [...]}. The list of attributes consists of tuples representing an attribute, e.g.:
                 (attributeName, attributeLabel, attributeDefinition)
    :param west_coord: western coordinate, optional; default: None
    :param east_coord: eastern coordinate, optional; default: None
    :param north_coord: northern coordinate, optional; default: None
    :param south_coord: southern coordinate, optional; default: None
    :return: None
    """

    eml = 'eml://ecoinformatics.org/eml-2.1.1'
    stmml = 'http://www.xml-cml.org/schema/stmml-1.1'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'
    schema_location = 'eml://ecoinformatics.org/eml-2.1.1 http://nis.lternet.edu/schemas/EML/eml-2.1.1/eml.xsd'

    eml_el = et.Element('{'+eml+'}eml', attrib={"{" + xsi + "}schemaLocation": schema_location},
                         nsmap={'eml': eml, 'stmml': stmml, 'xsi': xsi}, packageId='moorea.idea.template',
                         scope='system', system='moorea')

    if isinstance(data, dict):
        data = [data]

    for dataset in data:
        dataset_el = et.SubElement(eml_el, 'dataset')

        if 'organization_name' in dataset:
            creator_el = et.SubElement(dataset_el, 'creator')
            organization_name_el = et.SubElement(creator_el, 'organizationName')
            organization_name_el.text = dataset['organization_name']

        abstract_el = et.SubElement(dataset_el, 'abstract')
        para_el = et.SubElement(abstract_el, 'para')
        para_el.text = abstract

        coord_cond = any([west_coord, east_coord, north_coord, south_coord])
        dates_cond = ('begin_date' in dataset) or ('end_date' in dataset)

        if coord_cond or dates_cond:
            coverage_el = et.SubElement(dataset_el, 'coverage')

            if coord_cond:
                geographic_coverage_el = et.SubElement(coverage_el, 'geographicCoverage')
                bounding_coordinates_el = et.SubElement(geographic_coverage_el, 'boundingCoordinates')
                if west_coord or east_coord:
                    if west_coord:
                        west_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'westBoundingCoordinate')
                        west_bounding_coordinate_el.text = str(west_coord)
                    if east_coord:
                        east_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'eastBoundingCoordinate')
                        east_bounding_coordinate_el.text = str(east_coord)
                if north_coord or south_coord:
                    if north_coord:
                        north_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                      'northBoundingCoordinate')
                        north_bounding_coordinate_el.text = str(north_coord)
                    if south_coord:
                        south_bounding_coordinate_el = et.SubElement(bounding_coordinates_el,
                                                                      'southBoundingCoordinate')
                        south_bounding_coordinate_el.text = str(south_coord)

            if dates_cond:
                temporal_coverage_el = et.SubElement(coverage_el, 'temporalCoverage')
                range_of_dates_el = et.SubElement(temporal_coverage_el, 'rangeOfDates')
                begin_date_el = et.SubElement(range_of_dates_el, 'beginDate')
                calendar_date_begin_el = et.SubElement(begin_date_el, 'calendarDate')
                end_date_el = et.SubElement(range_of_dates_el, 'endDate')
                calendar_date_end_el = et.SubElement(end_date_el, 'calendarDate')
                if 'begin_date' in dataset and 'end_date' in dataset:
                    calendar_date_begin_el.text = dataset['begin_date']
                    calendar_date_end_el.text = dataset['end_date']
                elif 'begin_date' in dataset:
                    calendar_date_begin_el.text = dataset['begin_date']
                    calendar_date_end_el.text = calendar_date_begin_el.text
                else:
                    calendar_date_begin_el.text = dataset['end_date']
                    calendar_date_end_el.text = calendar_date_begin_el.text

        datatable_el = et.SubElement(dataset_el, 'dataTable')

        physical_el = et.SubElement(datatable_el, 'physical')
        object_name_el = et.SubElement(physical_el, 'objectName')
        object_name_el.text = dataset['object_name']

        attribute_list_el = et.SubElement(datatable_el, 'attributeList')

        for attribute in dataset['attributes_list']:
            attribute_el = et.SubElement(attribute_list_el, 'attribute')
            attribute_name_el = et.SubElement(attribute_el, 'attributeName')
            attribute_name_el.text = attribute[0]
            attribute_label_el = et.SubElement(attribute_el, 'attributeLabel')
            attribute_label_el.text = attribute[1]
            attribute_definition_el = et.SubElement(attribute_el, 'attributeDefinition')
            attribute_definition_el.text = attribute[2]

    et.ElementTree(eml_el).write(output_file_path, encoding='UTF-8', pretty_print=True, xml_declaration=True)

    return