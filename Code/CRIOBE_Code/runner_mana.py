# -*- coding: UTF-8 -*-
__author__ = 'JoergHerbel'

"""
This script creates XML files for the CRIOBE Polynesia Mana data ("Réseau de surveillance Polynesia Mana") described in:
http://observatoire.criobe.pf/document/DataStoreObs/meta/mana.pdf
The parameters were created manually by inspecting the metadata.
"""

import create_xml_file
import numpy as np
import os


abstract = 'Data collected by CRIOBE on corral reefs in the Pacific Ocean called during a survey called "Polynesia ' \
           'Mana"'

# Coordinates
longitude_deg = 149
longitude_arcmin = [51, 53, 54]
longitude_arcsec = [11, 206, 7]
longitude = -np.around(longitude_deg + np.array(longitude_arcmin, dtype=np.float32)/60
                       + np.array(longitude_arcsec, dtype=np.float32)/(60**2), 4)
latitude_deg = 17
latitude_arcmin = [28, 33, 28]
latitude_arcsec = [76, 906, 25]
latitude = -np.around(latitude_deg + np.array(latitude_arcmin, dtype=np.float32)/60
                      + np.array(latitude_arcsec, dtype=np.float32)/(60**2), 4)
num_coord = len(longitude)

# Dates
dates = [['1993-03-19', '1994-05-08', '1997-11-05', '1998-08-10', '2000-07-07', '2002-10-14', '2004-11-09',
          '2006-10-18', '2008-11'],
         ['1995-11-05', '1998-09-09', '2000-03-04', '2002-12-18', '2004-11-24', '2006-11-21'],
         ['1997-11-06', '1997-11-07', '1997-11-14', '1998-08-12', '1999-04-14', '1999-04-24', '2001-06-15',
          '2003-04-30', '2003-12-19', '2005-06-04', '2005-09-09', '2007-05-14', '2007-09-04']]

# Attributes
attributes_list1 = [(u'Recouvrement corallien (photographiques)', u'Coral cover in photoquadrat (%)', u'Percentage coral '
                     u'cover in 20 photoquadrats each covering an area of 1 m^2. Corals are differentiated according to '
                     u'their genera.'),
                    (u'Recouvrement corallien (manta tow)', u'Coral cover estimated using manta tow technique (%)',
                     u'Percentage coral cover estimated in 4 equally distributed sections using the manta tow technique. '
                     u'Each section being 500 m long.')]
attributes_list2 = [(u'Peuplements de poisson', u'Fish population', u'Fish population counted in 3 areas covering 250 m^2 '
                     u'each. The data was collected by human divers and consists of the genera and the size of individual '
                     u'fish. One of the transects covers the area of where photoquadrats for the coral survey are placed,'
                     u' the other 2 transects are located 25 m away from this transects, respectively.')]
attributes_list3 = [(u'Donées paysagères', u'Landscape data', u'Landscape data consisting of photographs taken at fixed'
                     u' positions and in fixed directions.')]


output_path = '../../Output/CRIOBE_Output/polynesia_mana/'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i in xrange(num_coord):
    name = 'mana' + '_longitude_' + str(longitude[i]) + '_latitude_' + str(latitude[i])
    num_dates = len(dates[i])
    datasets = [0] * num_dates
    for j in xrange(num_dates):
        year = int(dates[i][j][:4])
        if year < 2004:
            datasets[j] = {'organization_name': 'CRIOBE', 'begin_date': dates[i][j], 'end_date': dates[i][j],
                           'object_name': 'mana.pdf', 'attributes_list': attributes_list1}
        elif year < 2005:
            datasets[j] = {'organization_name': 'CRIOBE', 'begin_date': dates[i][j], 'end_date': dates[i][j],
                           'object_name': 'mana.pdf', 'attributes_list': attributes_list1+attributes_list2}
        else:
            datasets[j] = {'organization_name': 'CRIOBE', 'begin_date': dates[i][j], 'end_date': dates[i][j],
                           'object_name': 'mana.pdf',
                           'attributes_list': attributes_list1+attributes_list2+attributes_list3}
    create_xml_file.create_xml_file(output_path + name + '.xml', abstract, datasets, west_coord=longitude[i],
                                    east_coord=longitude[i], north_coord=latitude[i], south_coord=latitude[i])