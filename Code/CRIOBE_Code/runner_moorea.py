# -*- coding: UTF-8 -*-
__author__ = 'JoergHerbel'

"""
This script creates XML files for the CRIOBE data from marine protected areas on Moorea ("Les Aires Marines Protégées de
Moorea") described in:
http://observatoire.criobe.pf/document/DataStoreObs/meta/amp.pdf
The parameters were created manually by inspecting the metadata.
"""

import create_xml_file
import numpy as np
import os


abstract = 'Data collected by CRIOBE at marine protected areas on Moorea. All data was obtained by evaluating the ' \
           'respective quantity in 3 replicate transect quadrats (2 m x 25 m each) and taking the mean afterwards.'

# Coordinates
longitude_deg = 149
longitude_arcmin = [46.534, 46.312, 46.238, 46.614, 46.634, 46.644, 48.070, 47.747, 47.670, 45.493, 45.455, 45.279,
                    49.677, 49.695, 49.742, 54.647, 54.950, 55.129, 55.088, 55.380, 55.579, 54.005, 53.982, 53.985,
                    47.393, 47.151, 47.045, 50.191, 50.206, 50.222, 55.132, 55.404, 55.652, 53.555, 53.772, 53.864,
                    45.956, 45.706, 45.598]
longitude = -np.around(longitude_deg + np.array(longitude_arcmin)/60, 4)
latitude_deg = 17
latitude_arcmin = [32.668, 32.762, 32.948, 28.435, 28.402, 28.245, 35.423, 35.538, 35.772, 29.872, 29.979, 30.038,
                   28.073, 28.958, 28.596, 31.791, 31.926, 32.002, 30.658, 30.680, 30.706, 29.346, 29.145, 28.980,
                   33.745, 33.951, 34.151, 29.043, 28.817, 28.532, 34.155, 30.539, 30.313, 32.777, 33.056, 33.263,
                   30.229, 30.345, 30.405]
latitude = -np.around(latitude_deg + np.array(latitude_arcmin)/60, 4)

# Dates
dates = [('2004-07-27', '2004-08-06'), ('2005-02-15', '2005-02-26'), ('2005-08-12', '2005-08-26'),
         ('2006-01-07', '2006-01-21'), ('2006-07-04', '2006-07-11'), ('2007-01-29', '2007-02-07'),
         ('2007-07-26', '2007-08-03'), ('2008-02-10', '2008-02-26'), ('2008-08-13', '2008-08-20'),
         ('2009-02-04', '2009-02-12'), ('2009-09-01', '2009-09-09')]

# Attributes
attributes_list = [(u'Corail vivant', u'Cover by alive corals (genus level resolution, in %)', u'Percentage cover by '
                    u'alive corals resolved on the genus level. Considered genera: Acropora, Pocillopora, Astreopora, '
                    u'Cyphastrea, Favia, Fungia, Herpolitha, Leptastrea, Leptoseris, Lobophyllia, Millepora, Montastrea, '
                    u'Montipora, Pachyseris, Pavona, Porites, Psammocora, Sandalolitha.'),
                   (u'Corail mort', u'Cover by dead corals (%)', u'Percentage cover by corals dead less than one year.'),
                   (u'Macroalgue', u'Cover by macroalgae (genus level resolution, in %)', u'Percentage cover by '
                    u'macroalgae resolved on the genus level. Considered genera: Turbinaria, Sargassum, Halimeda, Padina,'
                    u' Boodlea, Cyanophycées, Dictyota, Caulerpa.'),
                   (u'Dalle', u'Cover by ground material (%)', u'Percentage cover by hard and compact ground material.'),
                   (u'Débris coralliens', u'Cover by coral debris (%)', u'Percentage cover by coral debris.'),
                   (u'Sable', u'Cover by sand (%)', u'Percentage cover by sand.'),
                   (u'Vase', u'Cover by alluvium (%)', u'Percentage cover by alluvium.'),
                   (u'Autre', u'Cover by all other organisms (%)', u'Percentage by all other organisms.'),
                   (u'Densité des invertébrés benthiques ', u'Density of benthic invertebrates (species level resolution)',
                    u'Density of benthic invertebrates resolved on the species level. Considered species: Tridacna maxima, '
                    u'Turbo marmoratus, Cassis cornuta, Trochus niloticus, Acanthaster planci, Bohadschia argus, '
                    u'Halodeima atra, Synapta sp., Telenota ananas, Tripneustes gratilla, Diadema sp., Echinothrix diadema.'),
                   (u'Peuplements de poissons', u'Fish counts and sizes (cm) (species level resolution)', u'Fish counts '
                    u'and sizes (in cm) resolved on species level. More than 160 fish species haven been taken into account.')]

datasets = [{'organization_name': 'CRIOBE', 'begin_date': dates[i][0], 'end_date': dates[i][1],
             'object_name': 'amp.pdf', 'attributes_list': attributes_list} for i in xrange(len(dates))]

output_path = '../.../Output/CRIOBE_Output/moorea/'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i in xrange(len(longitude)):
    name = 'moorea' + '_longitude_' + str(longitude[i]) + '_latitude_' + str(latitude[i])
    create_xml_file.create_xml_file(output_path + name + '.xml', abstract, datasets, west_coord=longitude[i],
                                    east_coord=longitude[i], north_coord=latitude[i], south_coord=latitude[i])