# -*- coding: UTF-8 -*-
__author__ = 'JoergHerbel'

"""
This script creates XML files for the CRIOBE environmental data ("Réseaux de suivi") described in:
http://observatoire.criobe.pf/document/DataStoreObs/meta/insu.pdf
The parameters were created manually by inspecting the metadata.
"""

import create_xml_file
import numpy as np
import os


abstract = 'Environmental data (biogeochemical and physical parameters) collected by CRIOBE on Moorea.'

###########################
# Biogeochemical parameters
###########################

# Coordinates
longitude_deg = 149
longitude_arcmin = 54
longitude_arcsec = [59.71, 03.08, 03.33]
longitude = -np.around(longitude_deg + longitude_arcmin/60 + np.array(longitude_arcsec)/(60**2), 5)

latitude_deg = 17
latitude_arcmin = [28, 29, 29]
latitude_arcsec = [59.71, 10.73, 22.61]
latitude = -np.around(latitude_deg + np.array(latitude_arcmin)/60 + np.array(latitude_arcsec)/(60**2), 5)

# Attributes
attributes_list = [(u'Phosphate', u'Phosphate concentration', u'Phosphate (PO4) concentration measured using persulfate '
                    u'oxidation digestion and PhosVer® 3 Phosphate Reagent on at least 1l of seawater collected in a '
                    u'plastic flacon'),
                   (u'Nitrates', u'Nitrate concentration', u'Nitrate (NO3) concentration measured using the cadmium '
                    u'reduction method on at least 1l of seawater collected in a plastic flacon'),
                   (u'Nitrites', u'Nitrite concentration', u'Nitrite (NO2) concentration measured using diazotization on '
                    u'at least 1l of seawater collected in a plastic flacon'),
                   (u'Carbonates', u'Carbonate concentration', u'Carbonate (CO3) concentration measured using ISO 9963-2 '
                    u'on at least 1l of seawater collected in a plastic flacon'),
                   (u'Silices', u'Silicon dioxide concentration', u'Carbonate (SiO3) concentration measured using the '
                    u'silicomolybdate method on at least 1l of seawater collected in a plastic flacon'),
                   (u'Ammonium', u'Ammonia concentration', u'Ammonia (NH3) concentration measured using the Nessler '
                    u'method on at least 1l of seawater collected in a plastic flacon')]

dataset = {'organization_name': 'CRIOBE', 'begin_date': '2008-01', 'end_date': '2015',
           'object_name': 'insu.pdf', 'attributes_list': attributes_list}

output_path = '/Users/JoergHerbel/Assistant_job/Output/CRIOBE_Output/environment/'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i in xrange(len(longitude)):
    name = 'env' + '_longitude_' + str(longitude[i]) + '_latitude_' + str(latitude[i])
    create_xml_file.create_xml_file(output_path + name + '.xml', abstract, dataset, west_coord=longitude[i],
                                    east_coord=longitude[i], north_coord=latitude[i], south_coord=latitude[i])


###################################
# Physical parameters and pH and O2
###################################

# Coordinates
longitude = -np.around(np.append(np.full(6, 149+53.985/60), 149+54.720/60), 5)

latitude_deg = 17
latitude_arcmin = [28.980, 28.960, 29, 28.890,  28.996, 28.940, 32.614]
latitude = -np.around(latitude_deg + np.array(latitude_arcmin)/60, 5)

# Attributes
attributes_list1 = [(u'T°', u'Temperature (°C)',
                     u'Temperature in °C measured once per hour with a Sea Bird SB16 and SB26')]
attributes_list2 = [(u'T°', u'Temperature (°C)',
                     u'Temperature in °C measured once per hour with a Sea Bird SB16 and SB26'),
                    (u'T°', u'Temperature (°C)',
                     u'Temperature in °C measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'houle', u'Sea condition',
                     u'Sea condition measured once every 15 min with a Sea Bird SB16andSB26'),
                    (u'marée', u'Tides', u'Tides measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'S°/°°', u'Salinity (per mille)',
                     u'Salinity in per mille measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'O2', u'Oxygen concentration',
                     u'Oxygen concentration measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'pH', u'pH', u'pH measured once every 15 min with a Sea Bird SB16 and SB26')]
attributes_list3 = [(u'T°', u'Temperature (°C)',
                     u'Temperature in °C measured once every 15 min with a Sea Bird SB16'),
                    (u'S°/°°', u'Salinity (per mille)',
                     u'Salinity in per mille measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'O2', u'Oxygen concentration',
                     u'Oxygen concentration measured once every 15 min with a Sea Bird SB16 and SB26'),
                    (u'pH', u'pH', u'pH measured once every 15 min with a Sea Bird SB16')]

output_path = '../../Output/CRIOBE_Output/environment/'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i in xrange(len(longitude)):
    if i <= 4:
        dataset = {'organization_name': 'CRIOBE', 'begin_date': '2008', 'end_date': 'ongoing',
                   'object_name': 'insu.pdf', 'attributes_list': attributes_list1}
    elif i == 5:
        dataset = {'organization_name': 'CRIOBE', 'begin_date': '2008', 'end_date': 'ongoing',
                   'object_name': 'insu.pdf', 'attributes_list': attributes_list2}
    else:
        dataset = {'organization_name': 'CRIOBE', 'begin_date': '2008', 'end_date': 'ongoing',
                   'object_name': 'insu.pdf', 'attributes_list': attributes_list3}
    name = 'env' + '_longitude_' + str(longitude[i]) + '_latitude_' + str(latitude[i])
    create_xml_file.create_xml_file(output_path + name + '.xml', abstract, dataset, west_coord=longitude[i],
                                    east_coord=longitude[i], north_coord=latitude[i], south_coord=latitude[i])
