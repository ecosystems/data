# -*- coding: UTF-8 -*-
__author__ = 'JoergHerbel'

"""
This script creates XML files for the CRIOBE survey on coral growth ("Recrutement coraux") described in:
http://observatoire.criobe.pf/document/DataStoreObs/meta/coraux.pdf
The parameters were created manually by inspecting the metadata.
"""

import create_xml_file
import numpy as np
import os

abstract = 'Data collected by CRIOBE on the growth of corals'

# Coordinates
longitude_deg = 149
longitude_arcmin = [55, 54, 51]
longitude_arcsec = [583, 68, 135]
longitude = -np.around(longitude_deg + np.array(longitude_arcmin, dtype=np.float32)/60
                       + np.array(longitude_arcsec, dtype=np.float32)/(60**2), 4)
latitude_deg = 17
latitude_arcmin = [30, 28, 28]
latitude_arcsec = [587, 987, 795]
latitude = -np.around(latitude_deg + np.array(latitude_arcmin, dtype=np.float32)/60
                      + np.array(latitude_arcsec, dtype=np.float32)/(60**2), 4)
num_coord = len(longitude)

# Dates
dates = [('2001-09', '2001-12'), ('2001-12', '2002-03'), ('2002-09', '2002-12'), ('2002-12', '2003-03'),
         ('2003-03', '2003-04'), ('2003-09', '2003-12'), ('2003-12', '2004-03'), ('2004-03', '2004-04'),
         ('2004-09', '2004-12'), ('2004-12', '2005-03'), ('2005-03', '2005-04'), ('2005-09', '2005-12'),
         ('2005-12', '2006-03'), ('2006-03', '2006-04'), ('2006-09', '2006-12'), ('2006-12', '2007-03'),
         ('2007-03', '2007-04'), ('2007-09', '2007-12'), ('2007-12', '2008-03'), ('2008-03', '2008-04'),
         ('2008-09', '2008-12'), ('2008-12', '2009-03'), ('2009-03', '2009-04')]

# Attributes
attributes_list1 = [(u'Abondance des juvéniles', u'Abundance of juvenile corals', u'Abundance of juvenile corals '
                     u'(between 1 and 5 cm in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the '
                     u'abundance was determined in 3 transect corridors (1 m x 10 m each).'),
                    (u'Composition taxonomique des juvéniles', u'Taxonomic composition of juvenile corals', u'Taxonomic '
                     u'composition of juvenile corals (between 1 and 5 cm in diameter) evaluated at depths of 6, 12 and '
                     u'18 m. For each depth, the composition was determined in 3 transect corridors (1 m x 10 m each).'),
                    (u'Contribution relative des différents taxons des juvéniles', u'Relative contribution of different '
                     u'taxons of juvenile corals', u'Relative contribution of different taxons of juvenile corals '
                     u'(between 1 and 5 cm in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the '
                     u'contributions were determined in 3 transect corridors (1 m x 10 m each).'),
                    (u'Abondance des adultes', u'Abundance of adult corals', u'Abundance of adult corals (at least 5 cm '
                     u'in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the abundance was determined '
                     u'in 3 transect corridors (1 m x 10 m each).'),
                    (u'Composition taxonomique des adultes', u'Taxonomic composition of adult corals', u'Taxonomic '
                     u'composition of adult corals (at least 5 cm in diameter) evaluated at depths of 6, 12 and '
                     u'18 m. For each depth, the composition was determined in 3 transect corridors (1 m x 10 m each).'),
                    (u'Contribution relative des différents taxons des adultes', u'Relative contribution of different '
                     u'taxons of adult corals', u'Relative contribution of different taxons of adult corals (at least 5 '
                     u'cm in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the contributions were '
                     u'determined in 3 transect corridors (1 m x 10 m each).')]
attributes_list2 = [(u'Abondance des recrues', u'Abundance of early stage corals', u'Abundance of early stage corals '
                     u'(less than 1 cm in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the '
                     u'abundance was determined by analyzing terra-cotta plates which were laid out previously, see: '
                     u'Mundy, C., 2000. An appraisal of methods used in coral recruitment studies. Coral Reefs 19, '
                     u'124–131.'),
                    (u'Composition taxonomique des recrues', u'Taxonomic composition of early stage corals', u'Taxonomic '
                     u'composition of very corals (less than 1 cm in diameter) evaluated at depths of 6, 12 and '
                     u'18 m. For each depth, the composition was determined by analyzing terra-cotta plates which were '
                     u'laid out previously, see: Mundy, C., 2000. An appraisal of methods used in coral recruitment '
                     u'studies. Coral Reefs 19, 124–131.'),
                    (u'Contribution relative des différents taxons des recrues', u'Relative contribution of different '
                     u'taxons of early stage corals', u'Relative contribution of different taxons of early stage corals '
                     u'(less than 1 cm in diameter) evaluated at depths of 6, 12 and 18 m. For each depth, the '
                     u'contributions was determined by analyzing terra-cotta plates which were laid out previously, see: '
                     u'Mundy, C., 2000. An appraisal of methods used in coral recruitment studies. Coral Reefs 19, 124–131.')]


datasets = []
for i in xrange(len(dates)):
    if dates[i][0][-2:] == '03':
        datasets.append({'organization_name': 'CRIOBE', 'begin_date': dates[i][0], 'end_date': dates[i][1],
                         'object_name': 'coraux.pdf', 'attributes_list': attributes_list1})
    else:
        datasets.append({'organization_name': 'CRIOBE', 'begin_date': dates[i][0], 'end_date': dates[i][1],
                         'object_name': 'coraux.pdf', 'attributes_list': attributes_list2})

output_path = '../../Output/CRIOBE_Output/corals/'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i in xrange(len(longitude)):
    name = 'coral' + '_longitude_' + str(longitude[i]) + '_latitude_' + str(latitude[i])
    create_xml_file.create_xml_file(output_path + name + '.xml', abstract, datasets, west_coord=longitude[i],
                                    east_coord=longitude[i], north_coord=latitude[i], south_coord=latitude[i])