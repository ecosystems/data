__author__ = 'JoergHerbel'

import re
import codecs


def txt_to_list(path, enc):
    """
    Read .txt-file and split at into single unicode strings

    :param path: location and filename
    :param enc: encoding
    :return: list of sublists of unicode strings corresponding to lines in input file
    """

    # Create list of sublists corresponding to lines in the file
    f = codecs.open(path, 'r', encoding=enc)
    data = f.readlines()

    lines = [re.split('\t', line) for line in data]  # Separate at tabs
    lines = [[entry.strip('\r\n') for entry in line] for line in lines]  # Cutoff linebreaks
    f.close()

    return lines


def create_chunks(lines, indices):
    """
    Group lines in input list according to entries with positions specified by indices

    :param lines: input list (must have same format as list returned by txtToList)
    :param indices: list of indices of the fields used for filtering
    :return: List of lists containing tuples corresponding to the values of the fields specified by the input indices
             and all lines whose corresponding fields have the values in the tuple
    """

    tuples = set()

    for line in lines:
        tuples.add(tuple([line[index] for index in indices]))

    tuples = list(tuples)
    num_tuples = len(tuples)

    chunks = [[t, [0]*(len(lines)-num_tuples+1)] for t in tuples]

    counters = [0]*num_tuples

    for line in lines:
        i = tuples.index(tuple([line[index] for index in indices]))
        chunks[i][1][counters[i]] = line
        counters[i] += 1

    for i in xrange(num_tuples):
        chunks[i][1] = chunks[i][1][:counters[i]]

    return chunks


def check_value(container, index):
    """
    Check whether container[index] is different from '\N' or ''

    :param container: contains unicode strings
    :param index: index of entry to be checked
    :return: Boolean value of condition
    """

    value = container[index]
    if (value != '') and (value != '\N') and (value != ' '):
        return True
    else:
        return False


def build_line(words, index_list):
    """
    Build a unicode string from a list of unicode strings

    :param words: list containing unicode strings from which string should be built
    :param index_list: indices of strings that should be used to build string
    :return: string containing indexed strings separated tabs and a linebreak
    """

    built_line = u''

    for n in index_list[:-1]:
        built_line += words[n] + u'\t'

    built_line += words[index_list[-1]] + u'\n'

    return built_line