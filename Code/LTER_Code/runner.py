__author__ = 'JoergHerbel'

"""
This script modifies (and splits if necessary) LTER XML files according to the data that is described in the XML file.
It also unifies the way the depth unit is stated in the geographicCoverage-part of the XML files. Since the LTER
datasets are not in one standardized format, dataset-specific input is required. The configurations below apply only
to the datasets stated in the comment above each call.
"""


from fix_geographic_coverage import fix_xml_file
from fix_geographic_coverage import fix_depth_unit
from fix_geographic_coverage import add_depths


# knb-lter-mcr.10
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.10', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((5,), (6,), (7,), (8,)), ((7,), (6,), (8,), (9,))], [';', ','], [1, 1], [26, 2],
             ignore_codes=[(('99999',), ('99999',), ('99999',), ('99999',)), ((), (), ('99999',), ())])


# knb-lter-mcr.12
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.12', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((3,), (2,), (), (9,))], [','], [1], [1])


# knb-lter-mcr.13
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.13', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((6,), (5,), (7,), (8,))], [','], [1], [4])


# knb-lter-mcr.21
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.21', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((4,), (3,), (), ())], [','], [1], [2])


# knb-lter-mcr.1034
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.1034', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((6,), (5,), (7,), (8,))], [','], [1], [1],
             ignore_codes=[(('99999',), ('99999',), ('99999',), ('99999',))])


# knb-lter-mcr.1037
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.1037', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((6,), (5,), (8,), (7,))], [','], [2], [2],
             ignore_codes=[((), (), ('99999',), ())])


# knb-lter-mcr.1040
add_depths('../../Input/LTER_Input/metadata/knb-lter-mcr.1040', '../../Input/LTER_Input/data/',
           '../../Output/LTER_Output/metadata/', [1, 1, 1], [',', ',', ','], [1, 1, 1])


# knb-lter-mcr.5003
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.5003', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((3,), (4,), (5,), (15,)), ((3,), (4,), (5,), (15,)), ((3,), (4,), (5,), (15,))], [',']*3, [1]*3, [None]*3,
             ignore_codes=[((), (), ('99999',), ()), ((), (), ('99999',), ()), ((), (), ('99999',), ())],
             mode=[0, 1, 2])


# knb-lter-mcr.5004
fix_xml_file('../../Input/LTER_Input/metadata/knb-lter-mcr.5004', '../../Input/LTER_Input/data/',
             '../../Output/LTER_Output/metadata/',
             [((3,), (4,), (5,), (15,)), ((3,), (4,), (5,), (15,))], [',']*2, [1]*2, [None]*2,
             ignore_codes=[((), (), ('99999',), ()), ((), (), ('99999',), ())], mode=[0, 1])


# Depth unit
fix_depth_unit('../../Output/LTER_Output/metadata/')


"""
The following call creates a csv-file containing the texts of the tags 'attributeName', 'attributeLabel' and
'attributeDefinition' in LTER XML files. The output file can be used to manually create a dictionary for new attribute
labels.
"""
"""
import get_attributes
get_attributes.write_attributes_to_csv('../../Input/LTER_Input/metadata/..',
                                       '../../Output/LTER_Output/metadata/attributes.csv')
"""

"""
This call replaces attribute labels in LTER XML files according to an external csv-file containing the old and the new
labels.
"""
"""
import get_attributes
get_attributes.replace_attribute_labels('../../Input/LTER_Input/metadata',
                                        '../../Output/LTER_Output/metadata/with_new_labels'
                                        '../../Output/LTER_Output/metadata/attributes_newLabel.csv')
"""