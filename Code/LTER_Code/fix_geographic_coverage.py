__author__ = 'JoergHerbel'

import csv
import lxml.etree as et
import numpy as np
import os
from itertools import chain


def get_column_combinations(path_input_file, column_indices, delimiter, start_row, name_index, ignore_codes=None):
    """
    Return all different combinations of values in rows containing numbers of a csv-file

    :param path_input_file: path of csv-file
    :param column_indices: indices of columns which are considered (grouped)
    :param delimiter: delimiter used when parsing file
    :param start_row: number of row where to start
    :param name_index: index of column containing names, can be None
    :param ignore_codes: codes which signal to ignore value
    :return: all different combinations and label dictionary if name_index is not None
    """

    if ignore_codes is None:
        ignore_codes = [()] * len(column_indices)

    with open(path_input_file, 'r') as f:
        rows = list(csv.reader(f, delimiter=delimiter))

    combinations = set()
    location_name_dict = {}

    for row in rows[start_row:]:
        row_entries = []
        for i in xrange(len(column_indices)):
            ind_group = column_indices[i]
            group = []
            for j in xrange(len(ind_group)):
                k = ind_group[j]
                try:
                    value = row[k]
                    if ignore_codes[i]:
                        if ignore_codes[i][j] == value:
                            continue
                    value = float(value)
                    if value > 0:
                        value = -value
                    group.append(value)
                except ValueError:
                    try:
                        s = row[k]
                        l = 0
                        while s[l].isdigit() or s[l] == '.':
                            l += 1
                        val1 = float(s[:l])
                        if val1 > 0:
                            val1 = -val1
                        while not (s[l].isdigit() or s[l] == '.'):
                            l += 1
                        val2 = float(s[l:])
                        if val2 > 0:
                            val2 = -val2
                        group.extend([val1, val2])
                    except IndexError:
                        pass
                except IndexError:
                    pass
            row_entries.append(tuple(group))
        if not np.any(np.isnan(list(chain.from_iterable(row_entries)))):
            t = tuple(row_entries)
            if name_index:
                if t not in combinations:
                    combinations.add(t)
                    location_name_dict[str(t)] = row[name_index]
            else:
                combinations.add(t)

    return combinations, location_name_dict


def fix_xml_file(path_input_file, data_dir, output_dir, column_indices, delimiters, start_rows, location_name_indices,
                 ignore_codes=None, mode='all_datasets'):
    """
    Sets appropriate locations for a LTER metadata XML file by splitting the file and adding/removing locations
    according to the data described by the file

    :param path_input_file: path of input XML file
    :param data_dir: directory where corresponding data is located
    :param output_dir: output directory
    :param column_indices: indices of columns which are considered in order to group and split the file; for each
                           dataset described by the XML file, one tuple of indices is necessary; format of tuple:
                           ((longitude indices), (latitude indices), (bottom depth indices), (sample depth indices))
                           (longitude indices) is again a tuple containing the indices of columns for the longitude
                           (either one index, in this case eastBoundingCoordinate and westBoundingCoordinate will be
                           equal in the output, or two indices, the first one for eastBoundingCoordinate and the
                           second one for westBoundingCoordinate)
    :param delimiters: delimiters used for parsing the data
    :param start_rows: rows where to start when extracting locations from data
    :param location_name_indices: indices of location names, can contain Nones
    :param ignore_codes: codes which signal to ignore values in data files
    :param mode: whether all datasets in the XML-file are considered, optional
    """

    name = os.path.basename(path_input_file)
    parser = et.XMLParser(remove_blank_text=True)
    tree = et.parse(path_input_file)
    data_tables = tree.xpath('//dataTable')

    list_locations_data = []
    list_location_namedicts = []

    if mode == 'all_datasets':
        ind = range(len(data_tables))
    else:
        ind = mode

    if ignore_codes is None:
        ignore_codes = [None] * len(ind)

    for i in ind:
        locations, name_dict = get_column_combinations(data_dir+data_tables[i].find('physical').find('objectName').text,
                                                       column_indices[i], delimiters[i], start_rows[i],
                                                       location_name_indices[i], ignore_codes=ignore_codes[i])
        list_locations_data.append(locations)
        list_location_namedicts.append(name_dict)

    # Group datasets
    groups = []
    for i in xrange(len(ind)):
        in_group = False
        for g in groups:
            if list_locations_data[g[0]] == list_locations_data[i]:
                g.append(i)
                in_group = True
                break
        if not in_group:
            groups.append([i])

    need_counting = (len(groups) > 1) or (mode != 'all_datasets')
    counter = 1

    # Take care of data sets not containing coordinates
    if mode != 'all_datasets':
        for i in ind:
            data_tables[i].getparent().remove(data_tables[i])
        # Remove Moorea as general location
        geographic_coverage_elements = tree.getroot().find('dataset').find('coverage').findall('geographicCoverage')
        if len(geographic_coverage_elements) > 1:
            for el in geographic_coverage_elements:
                geographic_description_el = el.find('geographicDescription')
                if geographic_description_el is not None and geographic_description_el.text == 'Moorea, French Polynesia':
                    el.getparent().remove(el)
        tree.write(output_dir+name+'_'+str(counter), encoding='UTF-8', pretty_print=True, xml_declaration=True)
        counter += 1

    for i in xrange(len(groups)):
        # Parse file again
        t = et.parse(path_input_file, parser)
        dat_tabs = t.xpath('//dataTable')

        # Remove datasets which are not in the group
        for j in set(range(len(dat_tabs))) - set(groups[i]):
            dat_tabs[j].getparent().remove(dat_tabs[j])

        # Remove all locations
        for el in t.getroot().find('dataset').find('coverage').findall('geographicCoverage'):
            el.getparent().remove(el)

        # Add locations
        for loc in list_locations_data[groups[i][0]]:
            if not loc[0] or not loc[1]:
                continue
            geographic_coverage_el = et.Element('geographicCoverage')
            try:
                geographic_coverage_el.set('id', list_location_namedicts[groups[i][0]][str(loc)])
            except KeyError:
                pass
            bounding_coordinates_el = et.SubElement(geographic_coverage_el, 'boundingCoordinates')
            west_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'westBoundingCoordinate')
            east_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'eastBoundingCoordinate')
            north_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'northBoundingCoordinate')
            south_bounding_coordinate_el = et.SubElement(bounding_coordinates_el, 'southBoundingCoordinate')
            west_bounding_coordinate_el.text = str(loc[0][0])
            try:
                east_bounding_coordinate_el.text = str(loc[0][1])
            except IndexError:
                east_bounding_coordinate_el.text = west_bounding_coordinate_el.text
            north_bounding_coordinate_el.text = str(loc[1][0])
            try:
                south_bounding_coordinate_el.text = str(loc[1][1])
            except IndexError:
                south_bounding_coordinate_el.text = north_bounding_coordinate_el.text
            if loc[2] or loc[3]:
                bounding_altitudes_el = et.SubElement(bounding_coordinates_el, 'boundingAltitudes', unit="meter")
                if loc[2]:
                    altitude_minimum_el = et.SubElement(bounding_altitudes_el, 'altitudeMinimum')
                    altitude_maximum_el = et.SubElement(bounding_altitudes_el, 'altitudeMaximum')
                    if len(loc[2]) == 2:
                        minimum = min(loc[2])
                        maximum = max(loc[2])
                        altitude_minimum_el.text = str(minimum)
                        altitude_maximum_el.text = str(maximum)
                    else:
                        altitude_minimum_el.text = str(loc[2][0])
                        altitude_maximum_el.text = '0.0'
                if loc[3]:
                    if len(loc[3]) == 2:
                        altitude_sampling_minimum_el = et.SubElement(bounding_altitudes_el, 'altitudeSamplingMinimum')
                        altitude_sampling_maximum_el = et.SubElement(bounding_altitudes_el, 'altitudeSamplingMaximum')
                        minimum = min(loc[3])
                        maximum = max(loc[3])
                        altitude_sampling_minimum_el.text = str(minimum)
                        altitude_sampling_maximum_el.text = str(maximum)
                    else:
                        altitude_sampling_el = et.SubElement(bounding_altitudes_el, 'altitudeSampling')
                        altitude_sampling_el.text = str(loc[3][0])

            t.getroot().find('dataset').find('coverage').append(geographic_coverage_el)

        if need_counting:
            output_name = name + '_' + str(counter)
            counter += 1
        else:
            output_name = name

        t.write(output_dir+output_name, encoding='UTF-8', pretty_print=True, xml_declaration=True)


def add_depths(path_input_file, data_dir, output_dir, depth_indices, delimiters, start_rows):
    """
    Split a LTER metadata XML file and add depths from data described by the file

    :param path_input_file: path of input XML file
    :param data_dir: directory where corresponding data is located
    :param output_dir: output directory
    :param depth_indices: indices of columns containing depth values
    :param delimiters: delimiters used for parsing the data
    :param start_rows: rows where to start when extracting depths from data
    :return:
    """

    name = os.path.basename(path_input_file)
    parser = et.XMLParser(remove_blank_text=True)
    tree = et.parse(path_input_file)
    data_tables = tree.xpath('//dataTable')
    need_counting = len(depth_indices) > 1

    for i in xrange(len(depth_indices)):
        depths = get_column_combinations(data_dir+data_tables[i].find('physical').find('objectName').text,
                                      [(depth_indices[i],)], delimiters[i], start_rows[i], None)[0]
        depths = sorted(map(lambda x: x[0][0], list(depths)))

        t = et.parse(path_input_file, parser)
        dat_tabs = t.xpath('//dataTable')

        r = range(len(depth_indices))
        r.remove(i)
        for j in r:
            dat_tabs[j].getparent().remove(dat_tabs[j])

        geographic_coverage_elements = t.getroot().find('dataset').find('coverage').findall('geographicCoverage')
        for j in r:
            geographic_coverage_elements[j].getparent().remove(geographic_coverage_elements[j])

        geographic_coverage_el = geographic_coverage_elements[i]
        bounding_coordinates_el = geographic_coverage_el.find('boundingCoordinates')
        bounding_altitudes_el = et.SubElement(bounding_coordinates_el, 'boundingAltitudes', unit="meter")

        for depth in depths:
            altitude_sampling_el = et.SubElement(bounding_altitudes_el, 'altitudeSampling')
            altitude_sampling_el.text = str(depth)

        if need_counting:
            output_name = name + '_' + str(i+1)
        else:
            output_name = name

        t.write(output_dir+output_name, encoding='UTF-8', pretty_print=True, xml_declaration=True)


def fix_depth_unit(input_dir):
    """
    Remove tags containing the units of depths in geographical coverages of LTER metadata and add the units as
    attributes of the boundingAltitudes-tag

    :param input_dir: folder where files to be fixed are located
    """

    filenames = [input_dir + filename for filename in os.listdir(input_dir) if not filename.startswith('.')]
    parser = et.XMLParser(remove_blank_text=True)

    for filename in filenames:
        tree = et.parse(filename, parser)

        altitude_elements = tree.xpath('//boundingAltitudes')

        for tag in altitude_elements:
            try:
                unit_el = tag.find('altitudeUnits')
                unit = unit_el.text
                tag.remove(unit_el)
                tag.set('unit', unit)
            except AttributeError:
                pass

        tree.write(filename, encoding='UTF-8', pretty_print=True, xml_declaration=True)
