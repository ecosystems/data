#!/bin/bash

# note: native access can be done through 
# http://mcr.lternet.edu/cgi-bin/showDataset.cgi?docid=<id of the dataset>

# list of datasets: http://mcr.lternet.edu/data/db/dataset.php
# xsd file: http://nis.lternet.edu/schemas/EML/eml-2.1.0/eml.xsd

LTER_BASEURL="http://metacat.lternet.edu/knb/metacat?action=read&qformat=xml&docid="
LTER_INDEX="http://mcr.lternet.edu/~gastilbuhl/share/avatar/inventory/select_docid.csv"

XML_DATE_RANGE="//dataset/coverage/temporalCoverage/rangeOfDates"
XML_DATE_POINT="//dataset/coverage/temporalCoverage/singleDateTime"

XML_COORD_W="//dataset/coverage/geographicCoverage/boundingCoordinates/westBoundingCoordinate"
XML_COORD_E="//dataset/coverage/geographicCoverage/boundingCoordinates/eastBoundingCoordinate"
XML_COORD_N="//dataset/coverage/geographicCoverage/boundingCoordinates/northBoundingCoordinate"
XML_COORD_S="//dataset/coverage/geographicCoverage/boundingCoordinates/southBoundingCoordinate"

XML_ENTITY="//dataset/dataTable/entityName"
XML_ENTITY_ALT="//dataset/otherEntity/entityName"

HTML="lter.php"

echo > $HTML
echo "Getting index..."
wget -q $LTER_INDEX -O lter.csv

list=`tail -n+2 lter.csv | head -n -1`

for i in $list 
do
    ix=(${i//,/ })
    id=${ix[0]}
    rev=${ix[1]}
    date=${ix[2]}
    ts=`date -d "$date" "+%s"`
    ts_old=`date -r $id +%s 2> /dev/null`
    [[ -z $ts_old ]] && ts_old=0

    if [ "$(($ts > $ts_old))" -eq "1" ]; then
        echo "Downloading $id"
        wget -q $LTER_BASEURL$id -O xml/$id
        touch -d "$date" xml/$id
    else
        echo "Skipping $id"
    fi

    dater=`xmllint --xpath "$XML_DATE_RANGE"        xml/$id 2> /dev/null`
    dates=`xmllint --xpath "$XML_DATE_POINT"        xml/$id 2> /dev/null`
    coords_w=`xmllint --xpath "$XML_COORD_W"        xml/$id 2> /dev/null`
    coords_e=`xmllint --xpath "$XML_COORD_E"        xml/$id 2> /dev/null`
    coords_n=`xmllint --xpath "$XML_COORD_N"        xml/$id 2> /dev/null`
    coords_s=`xmllint --xpath "$XML_COORD_S"        xml/$id 2> /dev/null`
    entities=`xmllint --xpath "$XML_ENTITY"         xml/$id 2> /dev/null`
    entities_alt=`xmllint --xpath "$XML_ENTITY_ALT" xml/$id 2> /dev/null`

    echo "<dataset>"                                             >> $HTML
    echo "    <dataset_id>$id</dataset_id>"                      >> $HTML
    echo "    <dataset_coords>"                                  >> $HTML
    echo "        $coords_w"                                     >> $HTML
    echo "        $coords_e"                                     >> $HTML
    echo "        $coords_n"                                     >> $HTML
    echo "        $coords_s"                                     >> $HTML
    echo "    </dataset_coords>"                                 >> $HTML
    echo "    <dataset_dates>"                                   >> $HTML
    echo "        $dater"                                        >> $HTML
    echo "        $dates"                                        >> $HTML
    echo "    </dataset_dates>"                                  >> $HTML
    echo "    <dataset_entities>"                                >> $HTML
    echo "        $entities"                                     >> $HTML
    echo "        $entities_alt"                                 >> $HTML
    echo "    </dataset_entities>"                               >> $HTML
    echo "</dataset>"                                            >> $HTML

    echo
done

rm lter.csv
