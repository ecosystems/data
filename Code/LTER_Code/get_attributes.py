__author__ = 'JoergHerbel'


import lxml.etree as et
import os
import sys
for path in [x[0] for x in os.walk(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))]:
    sys.path.append(path)
import csv_unicode


def write_attributes_to_csv(input_dir, path_output_file):
    """
    Parse all files in a folder (assumed to contain XML files only), find all tags called 'attribute' and write the
    texts of the subtags 'attributeName', 'attributeLabel' and 'attributeDefinition' of the attribute-tags to a csv-file

    :param input_dir: folder containing input files
    :param output_path: location where csv-file will be stored
    :return: None
    """

    filenames = [filename for filename in os.listdir(input_dir) if not filename.startswith('.')]
    attributes = []

    for filename in filenames:
        file_attributes = []

        for attribute in et.parse(os.path.join(input_dir, filename)).xpath('//attribute'):
            attribute_name = ''
            attribute_label = ''
            attribute_definition = ''

            for subelement in attribute.getchildren():
                if subelement.tag == 'attributeName':
                    attribute_name = subelement.text
                elif subelement.tag == 'attributeLabel':
                    attribute_label = subelement.text
                elif subelement.tag == 'attributeDefinition':
                    attribute_definition = subelement.text.replace('\n', ' ').replace('\t', ' ')
                    while '  ' in attribute_definition:
                        attribute_definition = attribute_definition.replace('  ', ' ')

            file_attributes.append([filename, attribute_name, attribute_label, attribute_definition])

        for file_attribute in file_attributes:
            contained = False
            for attr in attributes:
                if attr[1:] == file_attribute[1:]:
                    contained = True
                    if not file_attribute[0] in attr[0]:
                        attr[0] += ', ' + file_attribute[0]
                    break

            if not contained:
                attributes.append(file_attribute)

    # Sort primarily by filename and secondary by attributeName
    attributes = sorted(attributes, key=lambda x: (x[0], x[1]))
    for attr in attributes:
        attr.insert(-1, '')
    attributes.insert(0, ('filenames', 'attributeName', 'attributeLabel', 'new attributeLabel', 'attributeDefinition'))

    with open(path_output_file, 'w') as csv_file:
        w = csv_unicode.UnicodeWriter(csv_file)
        w.writerows(attributes)


def replace_attribute_labels(input_dir, output_dir, path_csv_file):
    """
    Replace the text of tags called 'attributeLabel' for all XML files in a directory (assumed to contain XML files
    only) according to a CSV file containing new labels and write the changed files to a directory

    :param input_dir: input directory
    :param output_dir: output directory
    :param path_csv_file: path of CSV file
    :return: None
    """

    with open(path_csv_file, 'r') as f:
        attributes_label_list = list(csv_unicode.UnicodeReader(f))

    filename_ind = attributes_label_list[0].index('filenames')
    name_ind = attributes_label_list[0].index('attributeName')
    label_ind = attributes_label_list[0].index('attributeLabel')
    definition_ind = attributes_label_list[0].index('attributeDefinition')
    new_label_ind = attributes_label_list[0].index('new attributeLabel')

    for filename in [filename for filename in os.listdir(input_dir) if not filename.startswith('.')]:
        attributes_label_list_file = [entry for entry in attributes_label_list if filename in entry[filename_ind] or
                                      entry[filename_ind] in filename]

        tree = et.parse(os.path.join(input_dir, filename))

        for attribute in tree.xpath('//attribute'):
            attribute_name = ''
            attribute_label = ''
            attribute_definition = ''

            for subelement in attribute.getchildren():
                if subelement.tag == 'attributeName':
                    attribute_name = subelement.text
                elif subelement.tag == 'attributeLabel':
                    attribute_label_tag = subelement
                    attribute_label = subelement.text
                elif subelement.tag == 'attributeDefinition':
                    attribute_definition = subelement.text.replace('\n', ' ').replace('\t', ' ')
                    while '  ' in attribute_definition:
                        attribute_definition = attribute_definition.replace('  ', ' ')

            for entry in attributes_label_list_file:
                if entry[name_ind] == attribute_name and entry[label_ind] == attribute_label \
                        and entry[definition_ind] == attribute_definition:
                            if entry[new_label_ind]:
                                attribute_label_tag.text = entry[new_label_ind]
                            break

        tree.write(os.path.join(output_dir, filename), encoding='UTF-8', pretty_print=True, xml_declaration=True)